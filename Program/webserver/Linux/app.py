from flask import Flask, current_app
import RPi.GPIO as GPIO

#MotorLeftBack = nekaj
#MotorLeftFront = nekaj
#MotorRightBack = nekaj
#MotorRightFront = nekaj

GPIO.setmode(GPIO.BOARD)
GPIO.setup(MotorLeftBack, GPIO.OUT)
GPIO.setup(MotorLeftFront, GPIO.OUT)
GPIO.setup(MotorRightBack, GPIO.OUT)
GPIO.setup(MotorRightFront, GPIO.OUT)

app = Flask(__name__)

@app.route('/')
def index():
    return current_app.send_static_file('index.html')
	
@app.route("/forward")
def forward():
	return "forward"
	GPIO.output()

@app.route("/back")
def back():
	return "back"
	GPIO.output()
	
@app.route("/left")
def left():
	return "left"
	GPIO.output()

@app.route("/right")
def right():
	return "right"
	GPIO.output()
	
if __name__ == '__main__':
   app.run(debug=True, port=80, host='0.0.0.0')
